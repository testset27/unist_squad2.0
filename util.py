import tensorflow as tf
import re
from collections import Counter
import string

'''
This file is taken and modified from R-Net by HKUST-KnowComp
https://github.com/HKUST-KnowComp/R-Net
'''
def get_record_parser(config, is_test=False):
    def parse(example):
        para_limit = config.test_para_limit if is_test else config.para_limit
        ques_limit = config.test_ques_limit if is_test else config.ques_limit
        char_limit = config.char_limit
        features = tf.parse_single_example(example,
                                           features={
                                               "context_idxs": tf.FixedLenFeature([], tf.string),
                                               "ques_idxs": tf.FixedLenFeature([], tf.string),
                                               "context_char_idxs": tf.FixedLenFeature([], tf.string),
                                               "ques_char_idxs": tf.FixedLenFeature([], tf.string),
                                               "y1": tf.FixedLenFeature([], tf.string),
                                               "y2": tf.FixedLenFeature([], tf.string),
                                               #@nguyent279(AddFeasibleAns):
                                               "plausible_y1": tf.FixedLenFeature([], tf.string),
                                               "plausible_y2": tf.FixedLenFeature([], tf.string),
                                               "id": tf.FixedLenFeature([], tf.int64), 
                                               "answerable": tf.FixedLenFeature([], tf.int64),
                                               "ELMo_context_char_idxs": tf.FixedLenFeature([], tf.string),
                                               "ELMo_ques_char_idxs": tf.FixedLenFeature([], tf.string),
											   "context_pos_idx": tf.FixedLenFeature([], tf.string),
											   "ques_pos_idx": tf.FixedLenFeature([], tf.string),
											   "context_ner_idx": tf.FixedLenFeature([], tf.string),
											   "ques_ner_idx": tf.FixedLenFeature([], tf.string)
                                           })
        context_idxs = tf.reshape(tf.decode_raw(
            features["context_idxs"], tf.int32), [para_limit])
        ques_idxs = tf.reshape(tf.decode_raw(
            features["ques_idxs"], tf.int32), [ques_limit])
        context_char_idxs = tf.reshape(tf.decode_raw(
            features["context_char_idxs"], tf.int32), [para_limit, char_limit])
        ques_char_idxs = tf.reshape(tf.decode_raw(
            features["ques_char_idxs"], tf.int32), [ques_limit, char_limit])
        y1 = tf.reshape(tf.decode_raw(
            features["y1"], tf.float32), [para_limit])
        y2 = tf.reshape(tf.decode_raw(
            features["y2"], tf.float32), [para_limit])
        #@nguyent279(AddFPlausibleAns):
        plausible_y1 = tf.reshape(tf.decode_raw(
            features["plausible_y1"], tf.float32), [para_limit])
        plausible_y2 = tf.reshape(tf.decode_raw(
            features["plausible_y2"], tf.float32), [para_limit])
        qa_id = features["id"]
        answerable = features["answerable"]
        ELMo_context_char_idxs = tf.reshape(tf.decode_raw(
            features["ELMo_context_char_idxs"], tf.int32), [para_limit+2, config.ELMo_limit])
        ELMo_ques_char_idxs = tf.reshape(tf.decode_raw(
            features["ELMo_ques_char_idxs"], tf.int32), [ques_limit+2, config.ELMo_limit])
        contex_pos_idxs = tf.reshape(tf.decode_raw(
           features["context_pos_idx"],tf.int32), [para_limit])
        ques_pos_idxs = tf.reshape(tf.decode_raw(
           features["ques_pos_idx"],tf.int32), [ques_limit])
        context_ner_idxs = tf.reshape(tf.decode_raw(
           features["context_ner_idx"],tf.int32), [para_limit])
        ques_ner_idxs = tf.reshape(tf.decode_raw(
           features["ques_ner_idx"],tf.int32), [ques_limit])

        return (context_idxs, ques_idxs, 
                context_char_idxs, ques_char_idxs, 
                y1, y2, 
                plausible_y1, plausible_y2,
                qa_id, 
                answerable, 
                ELMo_context_char_idxs, ELMo_ques_char_idxs,
				contex_pos_idxs, ques_pos_idxs, context_ner_idxs, ques_ner_idxs)
    return parse


def get_batch_dataset(record_file, parser, config):
    """get_batch_dataset
    ARGS:
        config
            is_bucket: If True, build a bucketing iterator (i.e., each sequence within a batch has the same sequence length but sequence length of different batches might be different?)
                       Source: https://stackoverflow.com/questions/48469779/how-to-handle-padding-when-using-sequence-length-parameter-in-tensorflow-dynamic
    """
    num_threads = tf.constant(config.num_threads, dtype=tf.int32)
    dataset = tf.data.TFRecordDataset(record_file).map(
        parser, num_parallel_calls=num_threads).shuffle(config.capacity).repeat()
    print(tf.data.TFRecordDataset(record_file).map(parser, num_parallel_calls=num_threads))
    if config.is_bucket:
        buckets = [tf.constant(num) for num in range(*config.bucket_range)]

        def key_func(context_idxs, ques_idxs, context_char_idxs, ques_char_idxs, y1, y2, plausible_y1, plausible_y2, qa_id, answerable, ELMo_context_char_idxs, ELMo_ques_char_idxs):
            c_len = tf.reduce_sum(
                tf.cast(tf.cast(context_idxs, tf.bool), tf.int32))
            t = tf.clip_by_value(buckets, 0, c_len)
            return tf.argmax(t)

        def reduce_func(key, elements):
            return elements.batch(config.batch_size)

        dataset = dataset.apply(tf.contrib.data.group_by_window(
            key_func, reduce_func, window_size=5 * config.batch_size)).shuffle(len(buckets) * 25)
    else:
        dataset = dataset.batch(config.batch_size)
    return dataset


def get_dataset(record_file, parser, config):
    num_threads = tf.constant(config.num_threads, dtype=tf.int32)
    dataset = tf.data.TFRecordDataset(record_file).map(
        parser, num_parallel_calls=num_threads).repeat().batch(config.batch_size)
    return dataset


def convert_tokens(eval_file, qa_id, pp1, pp2, a_preds):
    """convert_tokens
    """
    answer_dict = {}
    remapped_dict = {}
    for qid, p1, p2, answerable in zip(qa_id, pp1, pp2, a_preds):
        context = eval_file[str(qid)]["context"]
        spans = eval_file[str(qid)]["spans"]
        uuid = eval_file[str(qid)]["uuid"]
        start_idx = spans[p1][0]
        end_idx = spans[p2][1]
        answer_dict[str(qid)] = [] #If not answerable, answer_dict at qid is empty 
        if answerable:
            answer_dict[str(qid)] = context[start_idx: end_idx]
        remapped_dict[uuid] = context[start_idx: end_idx]            
    return answer_dict, remapped_dict


def evaluate(eval_file, answer_dict, is_analyze = False):
    f1 = exact_match = total = 0
    if is_analyze:
	    # Get every examples that and prediction is not matched with ground truths
        # {Key: [context, question, answers, prediction, f1]}
        error_dictionary = {}
    for key, value in answer_dict.items():
        total += 1
        ground_truths = eval_file[key]["answers"] #might be empty for SQuAD 2.0
        if is_analyze:
            question = eval_file[key]["question"]
            context = eval_file[key]["context"] 
        prediction = value 
        """
        exact_match += metric_max_over_ground_truths(
            exact_match_score, prediction, ground_truths)
        f1 += metric_max_over_ground_truths(f1_score,
                                            prediction, ground_truths)
        """
        em = metric_max_over_ground_truths(exact_match_score, prediction, ground_truths)
        exact_match += em
        fs = metric_max_over_ground_truths(f1_score, prediction, ground_truths)
        f1 += fs

        if is_analyze:
            qa = {'question': question,
		 	      'answer': ground_truths,
		 	      'prediction': prediction,
				  'em':em,
				  'f1':fs}
            if context not in error_dictionary:
                error_dictionary[context] = []
            error_dictionary[context].append(qa)
    exact_match = 100.0 * exact_match / total
    f1 = 100.0 * f1 / total
    return {'exact_match': exact_match, 'f1': f1}

def sanity_a_golds(a_golds):
    """Remove all answers that contain single punctuation 
    """
    return [a_gold for a_gold in a_golds if a_gold not in set(string.punctuation) ]

def evaluate2(eval_file, answer_dict):
    """evaluate2: Used for both Squad 1.1 and 2.0 
    """
    neg_count = 0
    na_score = 0 
    f1 = exact_match = total = 0
    for key, value in answer_dict.items():
        total += 1
        a_golds = eval_file[key]["answers"] #might be empty for SQuAD 2.0 
        a_golds = sanity_a_golds(a_golds)
        if len(a_golds) == 0:
            neg_count += 1 
        a_pred = value
        na_score += int( (len(a_golds) == 0) == (len(a_pred) == 0) )
        exact_match += metric_max_over_ground_truths(
            compute_exact, a_golds, a_pred)
        f1 += metric_max_over_ground_truths(compute_f1, a_golds, a_pred)
        # print('iter = {}, f1 = {}, neg_count = {}'.format(total, f1, neg_count))
        # if f1 > neg_count or f1 < neg_count:
        #     print("a_golds = {}, a_pred = {}".format(a_golds, a_pred))
        #     break
    exact_match = 100.0 * exact_match / total
    f1 = 100.0 * f1 / total
    na_acc = 100.0 * na_score / total 
    # print('Neg Rate = {}, NA acc = {}'.format(neg_count * 1. / total, na_acc))
    return {'exact_match': exact_match, 'f1': f1, 'na_acc': na_acc}

def normalize_answer(s):
    """WARNING: There is a case where one of the answers is '.' (this is not a good answer but rather it is a noise from crowdsourcing) and pred is []
    This should give 0 score but with this normalization, it gets score of 1 instead! 
    """

    def remove_articles(text):
        return re.sub(r'\b(a|an|the)\b', ' ', text)

    def white_space_fix(text):
        return ' '.join(text.split())

    def remove_punc(text):
        exclude = set(string.punctuation)
        return ''.join(ch for ch in text if ch not in exclude)

    def lower(text):
        return text.lower()

    return white_space_fix(remove_articles(remove_punc(lower(s))))

def get_tokens(s):
  if not s: return []
  return normalize_answer(s).split()

def f1_score(prediction, ground_truth):
    prediction_tokens = normalize_answer(prediction).split()
    ground_truth_tokens = normalize_answer(ground_truth).split()
    common = Counter(prediction_tokens) & Counter(ground_truth_tokens)
    num_same = sum(common.values())
    if num_same == 0:
        return 0
    precision = 1.0 * num_same / len(prediction_tokens)
    recall = 1.0 * num_same / len(ground_truth_tokens)
    f1 = (2 * precision * recall) / (precision + recall)
    return f1

def compute_f1(a_gold, a_pred):
  gold_toks = get_tokens(a_gold)
  pred_toks = get_tokens(a_pred) #if prediction of noa, pred_toks = []
  common = Counter(gold_toks) & Counter(pred_toks)
  num_same = sum(common.values())
  if len(gold_toks) == 0 or len(pred_toks) == 0:
    # If either is no-answer, then F1 is 1 if they agree, 0 otherwise
    return int(gold_toks == pred_toks)
  if num_same == 0:
    return 0
  precision = 1.0 * num_same / len(pred_toks)
  recall = 1.0 * num_same / len(gold_toks)
  f1 = (2 * precision * recall) / (precision + recall)
  return f1

def exact_match_score(prediction, ground_truth):
    return (normalize_answer(prediction) == normalize_answer(ground_truth))

def compute_exact(a_gold, a_pred):
    return int(get_tokens(a_gold) == get_tokens(a_pred))

def metric_max_over_ground_truths(metric_fn, a_golds, a_pred):
    scores_for_ground_truths = []
    if a_golds:
        for a_gold in a_golds:
            score = metric_fn(a_gold, a_pred)
            scores_for_ground_truths.append(score)
    else:
        score = metric_fn(a_golds, a_pred)
        scores_for_ground_truths.append(score)
    return max(scores_for_ground_truths)
