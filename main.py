import tensorflow as tf
import ujson as json
import numpy as np
from tqdm import tqdm
import os

'''
This file is taken and modified from R-Net by HKUST-KnowComp
https://github.com/HKUST-KnowComp/R-Net
'''


from model import Model
from demo import Demo
from util import get_record_parser, convert_tokens, evaluate, get_batch_dataset, get_dataset, evaluate2


def train(config):
    with open(config.word_emb_file, "r") as fh:
        word_mat = np.array(json.load(fh), dtype=np.float32)
    with open(config.char_emb_file, "r") as fh:
        char_mat = np.array(json.load(fh), dtype=np.float32)
    with open(config.train_eval_file, "r") as fh:
        train_eval_file = json.load(fh)
    with open(config.dev_eval_file, "r") as fh:
        dev_eval_file = json.load(fh)
    with open(config.dev_meta, "r") as fh:
        meta = json.load(fh)
    with open(config.pos_emb_file, "r") as fh:
        pos_mat = np.array(json.load(fh), dtype=np.float32)
    with open(config.ner_emb_file, "r") as fh:
        ner_mat = np.array(json.load(fh), dtype=np.float32)

    dev_total = meta["total"]
    print("Building model...")
    parser = get_record_parser(config)
    graph = tf.Graph()
    with graph.as_default() as g:
        train_dataset = get_batch_dataset(config.train_record_file, parser, config)
        dev_dataset = get_dataset(config.dev_record_file, parser, config)
        handle = tf.placeholder(tf.string, shape=[])
        iterator = tf.data.Iterator.from_string_handle(
            handle, train_dataset.output_types, train_dataset.output_shapes)
        train_iterator = train_dataset.make_one_shot_iterator()
        dev_iterator = dev_dataset.make_one_shot_iterator()

        model = Model(config, iterator, word_mat, char_mat, pos_mat, ner_mat, graph = g)

        sess_config = tf.ConfigProto(allow_soft_placement=True)
        sess_config.gpu_options.allow_growth = True

        loss_save = 100.0
        patience = 0
        best_f1 = 0.
        best_em = 0.
        learning_rate = config.learning_rate
        print('learning rate set to ',learning_rate)
        if not os.path.exists(os.path.join(config.log_dir, 'lr.npy')):
            np.save(os.path.join(config.save_dir, 'lr'), learning_rate)
        with tf.Session(config=sess_config) as sess:
            writer = tf.summary.FileWriter(config.log_dir)
            sess.run(tf.global_variables_initializer())
            saver = tf.train.Saver()
            train_handle = sess.run(train_iterator.string_handle())
            dev_handle = sess.run(dev_iterator.string_handle())
            if os.path.exists(os.path.join(config.save_dir, "checkpoint")): #If the learning was interrupted, next run will recover from the latest checkpoint
                # saver.restore(sess, tf.train.latest_checkpoint(config.save_dir))
                saver.restore(sess, os.path.join(config.save_dir, 'latest_model'))
                # recover the last learning rate: 
                learning_rate = np.load(os.path.join(config.save_dir), 'lr.npy').tolist()
                print("Recover from the last state: global_step = {}, lr = {}".format(model.global_step, learning_rate))
            else:
                print("Not checkpointed yet: initial lr = {}".format(learning_rate))
            global_step = max(sess.run(model.global_step), 1)

            for _ in tqdm(range(global_step, config.num_steps + 1)):
                global_step = sess.run(model.global_step) + 1
                loss, train_op = sess.run([model.loss, model.train_op], feed_dict={
                                           handle: train_handle, model.dropout: config.dropout, model.lr: learning_rate})
                if global_step % config.period == 0:
                    loss_sum = tf.Summary(value=[tf.Summary.Value(
                        tag="model/loss", simple_value=loss), ])
                    writer.add_summary(loss_sum, global_step)
                
                if global_step % config.checkpoint == 0:

                    # print("Evaluate on train...\n")
                    _, summ = evaluate_batch(
                        model, config.val_num_batches, train_eval_file, sess, "train", handle, train_handle) # to update train summary 
                    for s in summ:
                        writer.add_summary(s, global_step)

                    # print("Evaluate on dev ...\n")
                    metrics, summ = evaluate_batch(
                        model, dev_total // config.batch_size + 1, dev_eval_file, sess, "dev", handle, dev_handle)

                    dev_f1 = metrics["f1"]
                    dev_em = metrics["exact_match"]
                    if dev_f1 < best_f1 and dev_em < best_em:
                        patience += 1
                        if patience > config.early_stop:
                            #learning_rate = config.learning_rate * (0.5**(global_step/30000))
                            learning_rate = learning_rate /2
                            print('learning rate changed from ',learning_rate*2, 'to ',learning_rate)
                            patience = 0
                            #Save new learning rate 
                            np.save(os.path.join(config.save_dir, 'lr'), learning_rate)
                    else:
                        patience = 0
                        best_em = max(best_em, dev_em)
                        best_f1 = max(best_f1, dev_f1)

                        #@nguyent279(COMMENT): save the best model 
                        filename = os.path.join(config.save_dir, "best_model")
                        saver.save(sess, filename)

                    for s in summ:
                        writer.add_summary(s, global_step)
                    writer.flush()
                    #@(nguyent2792): tf.train.Saver automatically deletes old checkpoints and keep a certain number of the newest ones (default: 5)
                    # To be able to save the checkpoint for the best validation, my trick: just save two checkpoints - the latest checkpoint and the one for the best validation
                    filename = os.path.join(config.save_dir, "latest_model")
                    saver.save(sess, filename)


def evaluate_batch(model, num_batches, eval_file, sess, data_type, handle, str_handle):
    """evaluate_batch

    ARG:
        eval_file: ground-truth answers from the dev set 
            "answers": some of them are empty. Does it indicate an unansweable question? -> YES
            "spans": What the hell is this??
    RETURN:
    """
    answer_dict = {}
    losses = []
    for _ in tqdm(range(1, num_batches + 1)):
        qa_id, loss, yp1, yp2, answerable_preds = sess.run(
            [model.qa_id, model.loss, model.yp1, model.yp2, model.answerable_preds], feed_dict={handle: str_handle})
        answer_dict_, _ = convert_tokens(
            eval_file, qa_id.tolist(), yp1.tolist(), yp2.tolist(), answerable_preds.tolist())
        answer_dict.update(answer_dict_)
        losses.append(loss)
    loss = np.mean(losses)

    metrics = evaluate2(eval_file, answer_dict)
    metrics["loss"] = loss
    loss_sum = tf.Summary(value=[tf.Summary.Value(
        tag="{}/loss".format(data_type), simple_value=metrics["loss"]), ])
    f1_sum = tf.Summary(value=[tf.Summary.Value(
        tag="{}/f1".format(data_type), simple_value=metrics["f1"]), ])
    em_sum = tf.Summary(value=[tf.Summary.Value(
        tag="{}/em".format(data_type), simple_value=metrics["exact_match"]), ])
    na_sum = tf.Summary(value=[tf.Summary.Value(
        tag="{}/na_acc".format(data_type), simple_value=metrics["na_acc"]), ])
    return metrics, [loss_sum, f1_sum, em_sum, na_sum]


def demo(config):
    with open(config.word_emb_file, "r") as fh:
        word_mat = np.array(json.load(fh), dtype=np.float32)
    with open(config.char_emb_file, "r") as fh:
        char_mat = np.array(json.load(fh), dtype=np.float32)
    with open(config.test_meta, "r") as fh:
        meta = json.load(fh)
    with open(config.pos_emb_file, "r") as fh:
        pos_mat = np.array(json.load(fh), dtype=np.float32)
    with open(config.ner_emb_file, "r") as fh:
        ner_mat = np.array(json.load(fh), dtype=np.float32)

    model = Model(config, None, word_mat, char_mat, pos_mat, ner_mat, trainable=False, demo = True)
    demo = Demo(model, config)


def test(config, is_analyze = False):
    with open(config.word_emb_file, "r") as fh:
        word_mat = np.array(json.load(fh), dtype=np.float32)
    with open(config.char_emb_file, "r") as fh:
        char_mat = np.array(json.load(fh), dtype=np.float32)
    with open(config.test_eval_file, "r") as fh:
        eval_file = json.load(fh)
    with open(config.test_meta, "r") as fh:
        meta = json.load(fh)
    with open(config.pos_emb_file, "r") as fh:
        pos_mat = np.array(json.load(fh), dtype=np.float32)
    with open(config.ner_emb_file, "r") as fh:
        ner_mat = np.array(json.load(fh), dtype=np.float32)


    total = meta["total"]

    graph = tf.Graph()
    print("Loading model...")
    with graph.as_default() as g:
        test_batch = get_dataset(config.test_record_file, get_record_parser(
            config, is_test=True), config).make_one_shot_iterator()

        model = Model(config, test_batch, word_mat, char_mat, pos_mat, ner_mat, trainable=False, graph = g)

        sess_config = tf.ConfigProto(allow_soft_placement=True)
        sess_config.gpu_options.allow_growth = True

        with tf.Session(config=sess_config) as sess:
            sess.run(tf.global_variables_initializer())
            saver = tf.train.Saver()
            if config.use_checkpoint_of_best_validation:
                checkpoint_path =  os.path.join(config.train_dir, config.model_name, "model/best_model")
                print("Restore the model from {}".format(checkpoint_path))
                saver.restore(sess, checkpoint_path)
            else:
                print("Restore the model from the latest checkpoint.")
                # saver.restore(sess, tf.train.latest_checkpoint(config.save_dir))
                checkpoint_path =  os.path.join(config.train_dir, config.model_name, "model/latest_model")
                saver.restore(sess, checkpoint_path)
            if config.decay < 1.0:
                sess.run(model.assign_vars)
            losses = []
            answer_dict = {}
            remapped_dict = {}
            for step in tqdm(range(total // config.batch_size + 1)):
                qa_id, loss, yp1, yp2, answerable_preds = sess.run(
                    [model.qa_id, model.loss, model.yp1, model.yp2, model.answerable_preds])
                answer_dict_, remapped_dict_ = convert_tokens(
                    eval_file, qa_id.tolist(), yp1.tolist(), yp2.tolist(), answerable_preds.tolist())
                answer_dict.update(answer_dict_)
                remapped_dict.update(remapped_dict_)
                losses.append(loss)
            loss = np.mean(losses)
            metrics = evaluate2(eval_file, answer_dict)
            with open(config.answer_file, "w") as fh:
                json.dump(remapped_dict, fh)
            if is_analyze:
                with open(config.error_file, "w") as fh:
                    json.dump(metrics['error_dictionary'], fh)
            print("EM: {}, F1: {}, NA_acc: {}".format(
                metrics['exact_match'], metrics['f1'], metrics['na_acc']))
