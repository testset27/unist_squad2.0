import numpy as np 
import tensorflow as tf
from layers import initializer, regularizer, residual_block, highway, conv, mask_logits, trilinear, total_params, optimized_trilinear_for_attention, _linear, BiGRU, stable_softmax

VERY_NEGATIVE_BUT_STABLE_ENOUGH = -1e30
POSITIVE_BUT_CLOSE_TO_ZERO = 1e-30

import os
from bilm_tf.bilm import Batcher, BidirectionalLanguageModel, weight_layers

def pad_up_to(t, max_in_dims):
    s = tf.shape(t)
    paddings = [[0, m-s[i]] for (i,m) in enumerate(max_in_dims)]
    return tf.pad(t, paddings, 'CONSTANT')

class Model(object):
    def __init__(self, config, batch, word_mat=None, char_mat=None, pos_mat = None, ner_mat = None, trainable=True, opt=True, demo = False, graph = None):
        self.config = config
        self.demo = demo
        self.graph = graph if graph is not None else tf.Graph()
        with self.graph.as_default():

            self.global_step = tf.get_variable('global_step', shape=[], dtype=tf.int32,
                                               initializer=tf.constant_initializer(0), trainable=False)
            self.dropout = tf.placeholder_with_default(0.0, (), name="dropout")
            if self.demo:
                self.c = tf.placeholder(tf.int32, [None, config.test_para_limit],"context")
                self.q = tf.placeholder(tf.int32, [None, config.test_ques_limit],"question")
                self.ch = tf.placeholder(tf.int32, [None, config.test_para_limit, config.char_limit],"context_char")
                self.qh = tf.placeholder(tf.int32, [None, config.test_ques_limit, config.char_limit],"question_char")
                self.y1 = tf.placeholder(tf.int32, [None, config.test_para_limit],"answer_index1")
                self.y2 = tf.placeholder(tf.int32, [None, config.test_para_limit],"answer_index2")
                #@nguyent279(AddPlausibleAns):
                self.plausible_y1 = tf.placeholder(tf.int32, [None, config.test_para_limit],"plausible_answer_index1")
                self.plausible_y2 = tf.placeholder(tf.int32, [None, config.test_para_limit],"plausible_answer_index2")

                # @(soonjae) Additional ELMo inputs 
                self.Ech = tf.placeholder(tf.int32, [None, config.test_para_limit+2, config.ELMo_limit],"ELMo_context_char_idxs")
                self.Eqh = tf.placeholder(tf.int32, [None, config.test_ques_limit+2, config.ELMo_limit],"ELMo_ques_char_idx")

                # Additional linguisitcs inputs
                self.cpos = tf.placeholder(tf.int32, [None, config.test_para_limit],"question")
                self.qpos = tf.placeholder(tf.int32, [None, config.test_ques_limit],"question")
                self.cner = tf.placeholder(tf.int32, [None, config.test_para_limit],"question")
                self.qner = tf.placeholder(tf.int32, [None, config.test_ques_limit],"question")

            else:
                #@(nguyent279)(AddPlausibleAns)
                self.c, self.q, self.ch, self.qh, self.y1, self.y2, self.plausible_y1, self.plausible_y2, \
                self.qa_id, self.answerable_labels, self.Ech, self.Eqh, self.cpos, self.qpos, self.cner, self.qner \
                                                                                                                = batch.get_next()

            # self.word_unk = tf.get_variable("word_unk", shape = [config.glove_dim], initializer=initializer())
            self.word_mat = tf.get_variable("word_mat", initializer=tf.constant(
                word_mat, dtype=tf.float32), trainable=False)
            self.char_mat = tf.get_variable(
                "char_mat", initializer=tf.constant(char_mat, dtype=tf.float32), trainable=True)
            self.pos_mat =  tf.get_variable("pos_mat", initializer=tf.constant(
                pos_mat, dtype=tf.float32), trainable=True)			    
            self.ner_mat =  tf.get_variable("ner_mat", initializer=tf.constant(
                ner_mat, dtype=tf.float32), trainable=True)			    
            
            self.c_mask = tf.cast(self.c, tf.bool)
            self.q_mask = tf.cast(self.q, tf.bool)
            self.c_len = tf.reduce_sum(tf.cast(self.c_mask, tf.int32), axis=1)
            self.q_len = tf.reduce_sum(tf.cast(self.q_mask, tf.int32), axis=1)

            # @(soonjae) Code for the ELMo
            datadir = os.path.join('bilm_tf','tests', 'fixtures', 'model')
            options_file = os.path.join(datadir, 'pretrained_options.json')
            weight_file = os.path.join(datadir, 'pretrained_lm_weights.hdf5') 
            self.bilm = BidirectionalLanguageModel(options_file, weight_file)



            if opt:
                #@thanhnt(COMMENT): This opt adds 'bucketing feature' to each batch
                #Samples with a batch are padded to the same sequence length but samples across different batches might have different padding lengths
                N, CL = config.batch_size if not self.demo else 1, config.char_limit
                self.c_maxlen = tf.reduce_max(self.c_len)
                self.q_maxlen = tf.reduce_max(self.q_len)
                self.c = tf.slice(self.c, [0, 0], [N, self.c_maxlen])
                self.q = tf.slice(self.q, [0, 0], [N, self.q_maxlen])
                self.c_mask = tf.slice(self.c_mask, [0, 0], [N, self.c_maxlen]) # @thanhnt(COMMENT): real sequence lengths (but cut off by c_maxlen) 
                self.q_mask = tf.slice(self.q_mask, [0, 0], [N, self.q_maxlen])
                self.ch = tf.slice(self.ch, [0, 0, 0], [N, self.c_maxlen, CL])
                self.qh = tf.slice(self.qh, [0, 0, 0], [N, self.q_maxlen, CL])
                self.y1 = tf.slice(self.y1, [0, 0], [N, self.c_maxlen])
                self.y2 = tf.slice(self.y2, [0, 0], [N, self.c_maxlen])
                #nguyent279(AddPlausibleAns)
                self.plausible_y1 = tf.slice(self.plausible_y1, [0, 0], [N, self.c_maxlen])
                self.plausible_y2 = tf.slice(self.plausible_y2, [0, 0], [N, self.c_maxlen])
                self.Ech = tf.slice(self.Ech, [0, 0, 0], [N, self.c_maxlen+2, config.ELMo_limit])
                self.Eqh = tf.slice(self.Eqh, [0, 0, 0], [N, self.q_maxlen+2, config.ELMo_limit])
                self.cpos = tf.slice(self.cpos, [0, 0], [N, self.c_maxlen])
                self.qpos = tf.slice(self.qpos, [0, 0], [N, self.q_maxlen])
                self.cner = tf.slice(self.cner, [0, 0], [N, self.c_maxlen])
                self.qner = tf.slice(self.qner, [0, 0], [N, self.q_maxlen])
            else:
                self.c_maxlen, self.q_maxlen = config.para_limit, config.ques_limit

            self.ch_len = tf.reshape(tf.reduce_sum(
                tf.cast(tf.cast(self.ch, tf.bool), tf.int32), axis=2), [-1])
            self.qh_len = tf.reshape(tf.reduce_sum(
                tf.cast(tf.cast(self.qh, tf.bool), tf.int32), axis=2), [-1])

            self.forward()
            total_params()

            if trainable:
                #self.lr = tf.minimum(config.learning_rate, 0.001 / tf.log(999.) * tf.log(tf.cast(self.global_step, tf.float32) + 1))
                self.lr = tf.placeholder_with_default(config.learning_rate, (), name="learning_reate") 
                self.lr_ = tf.minimum(self.lr, self.lr/tf.log(999.) * tf.log(tf.cast(self.global_step, tf.float32) + 1))
                self.opt = tf.train.AdamOptimizer(learning_rate = self.lr_, beta1 = 0.8, beta2 = 0.999, epsilon = 1e-7)
                grads = self.opt.compute_gradients(self.loss)
                gradients, variables = zip(*grads)
                capped_grads, _ = tf.clip_by_global_norm(
                    gradients, config.grad_clip)
                self.train_op = self.opt.apply_gradients(
                    zip(capped_grads, variables), global_step=self.global_step)

    def forward(self):
        config = self.config
        N, PL, QL, CL, d, dc, nh = config.batch_size if not self.demo else 1, self.c_maxlen, self.q_maxlen, config.char_limit, config.hidden, config.char_dim, config.num_heads
        if config.use_ELMo:
            Ec_emb_op = self.bilm(self.Ech) 
            Eq_emb_op = self.bilm(self.Eqh)
            Ec_emb = weight_layers('input', Ec_emb_op, l2_coef=0.0)['weighted_op'] 
            with tf.variable_scope('', reuse=True):
                Eq_emb = weight_layers('input', Eq_emb_op, l2_coef=0.0)['weighted_op']
            cE_emb = tf.nn.dropout(Ec_emb, 1.0 - self.dropout) 
            qE_emb = tf.nn.dropout(Eq_emb, 1.0 - self.dropout)

			
        with tf.variable_scope("Input_Embedding_Layer"):
            ch_emb = tf.reshape(tf.nn.embedding_lookup(
                self.char_mat, self.ch), [N * PL, CL, dc])
            qh_emb = tf.reshape(tf.nn.embedding_lookup(
                self.char_mat, self.qh), [N * QL, CL, dc])
            ch_emb = tf.nn.dropout(ch_emb, 1.0 - 0.5 * self.dropout)
            qh_emb = tf.nn.dropout(qh_emb, 1.0 - 0.5 * self.dropout)

			# Bidaf style conv-highway encoder
            ch_emb = conv(ch_emb, d,
                bias = True, activation = tf.nn.relu, kernel_size = 5, name = "char_conv", reuse = None)
            qh_emb = conv(qh_emb, d,
                bias = True, activation = tf.nn.relu, kernel_size = 5, name = "char_conv", reuse = True)
          
            ch_emb = tf.reduce_max(ch_emb, axis = 1)
            qh_emb = tf.reduce_max(qh_emb, axis = 1)

            ch_emb = tf.reshape(ch_emb, [N, PL, ch_emb.shape[-1]])
            qh_emb = tf.reshape(qh_emb, [N, QL, ch_emb.shape[-1]])

            c_emb = tf.nn.dropout(tf.nn.embedding_lookup(self.word_mat, self.c), 1.0 - self.dropout)
            q_emb = tf.nn.dropout(tf.nn.embedding_lookup(self.word_mat, self.q), 1.0 - self.dropout)

            cins = [c_emb]
            qins = [q_emb]
            if config.use_ELMo: #@nguyent279: Replace chararater embedding with Elmo
                cins.append(cE_emb)
                qins.append(qE_emb)
                if config.use_charEmb_along_w_Elmo: 
                    cins.append(ch_emb)
                    qins.append(qh_emb)
            else:
                cins.append(ch_emb)
                qins.append(qh_emb)
            # @(soonjae) pos, ner features
            if config.use_POS:
                cpos_emb = tf.nn.dropout(tf.nn.embedding_lookup(self.pos_mat, self.cpos), 1.0 - self.dropout) 
                qpos_emb = tf.nn.dropout(tf.nn.embedding_lookup(self.pos_mat, self.qpos), 1.0 - self.dropout) 
                cins.append(cpos_emb)
                qins.append(qpos_emb)

            if config.use_NER:
                cner_emb = tf.nn.dropout(tf.nn.embedding_lookup(self.ner_mat, self.cner), 1.0 - self.dropout) 
                qner_emb = tf.nn.dropout(tf.nn.embedding_lookup(self.ner_mat, self.qner), 1.0 - self.dropout) 
                cins.append(cner_emb)
                qins.append(qner_emb)

            # @(soonjae) 
            c_emb = tf.concat(cins, axis=2)
            q_emb = tf.concat(qins, axis=2)


            c_emb = highway(c_emb, size = d, scope = "highway", dropout = self.dropout, reuse = None)
            q_emb = highway(q_emb, size = d, scope = "highway", dropout = self.dropout, reuse = True)

            

        with tf.variable_scope("Embedding_Encoder_Layer"):
            c = residual_block(c_emb,
                num_blocks = 1,
                num_conv_layers = 4,
                kernel_size = 7,
                mask = self.c_mask,
                num_filters = d,
                num_heads = nh,
                seq_len = self.c_len,
                scope = "Encoder_Residual_Block",
                bias = False,
                dropout = self.dropout)
            q = residual_block(q_emb,
                num_blocks = 1,
                num_conv_layers = 4,
                kernel_size = 7,
                mask = self.q_mask,
                num_filters = d,
                num_heads = nh,
                seq_len = self.q_len,
                scope = "Encoder_Residual_Block",
                reuse = True, # Share the weights between passage and question
                bias = False,
                dropout = self.dropout)

        with tf.variable_scope("Context_to_Query_Attention_Layer"):
            # C = tf.tile(tf.expand_dims(c,2),[1,1,self.q_maxlen,1])
            # Q = tf.tile(tf.expand_dims(q,1),[1,self.c_maxlen,1,1])
            # S = trilinear([C, Q, C*Q], input_keep_prob = 1.0 - self.dropout)
            S = optimized_trilinear_for_attention([c, q], self.c_maxlen, self.q_maxlen, input_keep_prob = 1.0 - self.dropout)
            mask_q = tf.expand_dims(self.q_mask, 1)
            S_ = tf.nn.softmax(mask_logits(S, mask = mask_q))
            mask_c = tf.expand_dims(self.c_mask, 2)
            S_T = tf.transpose(tf.nn.softmax(mask_logits(S, mask = mask_c), dim = 1),(0,2,1))
            self.c2q = tf.matmul(S_, q)
            self.q2c = tf.matmul(tf.matmul(S_, S_T), c)
            self.attention_outputs = [c, self.c2q, c * self.c2q, c * self.q2c]
            #@nguyent279(AddFusion): Add early fusion
            self.early_fusing_signal = self.c2q * self.q2c
        
        #@thanhnt(AddFeature): Add a decision layer that learns to decide if a question is answerable
        # if self.config.use_answerable:
        #     with tf.variable_scope("Decision_Layer"):
        #         inputs = tf.concat(attention_outputs, axis = -1)
        #         shp = inputs.get_shape().as_list()
        #         inputs_padded = pad_up_to(inputs, [shp[0], self.config.para_limit, shp[2]])
        #         self.monitor = inputs_padded
        #         inputs_2d = tf.reshape(inputs_padded, (self.config.batch_size, self.config.para_limit * 384)) 
        #         self.answerable_logits = _linear(inputs_2d, 1, bias=True, scope='answerable_prob' ) 
        #         self.answerable_preds = tf.sigmoid( self.answerable_logits ) >= 0.5  

        with tf.variable_scope("Model_Encoder_Layer"):
            inputs = tf.concat(self.attention_outputs, axis = -1)
            self.enc = [conv(inputs, d, name = "input_projection")] #@thanhnt(QUESTION): Why he calls 'conv' as if it is a linear projection? 
            for i in range(3):
                if i % 2 == 0: # dropout every 2 blocks
                    self.enc[i] = tf.nn.dropout(self.enc[i], 1.0 - self.dropout)
                #nguyent279(AddFusion): fuse the early signal to block 2 and block 3
                if self.config.use_fusion and i > 0:
                    _enc_input = self.enc[i] * self.early_fusing_signal
                else:
                    _enc_input = self.enc[i] 
                self.enc.append(
                    residual_block( _enc_input,
                        num_blocks = 7,
                        num_conv_layers = 2,
                        kernel_size = 5,
                        mask = self.c_mask,
                        num_filters = d,
                        num_heads = nh,
                        seq_len = self.c_len,
                        scope = "Model_Encoder",
                        bias = False,
                        reuse = True if i > 0 else None,
                        dropout = self.dropout)
                    )

        #@nguyent2792(AddFeature): Add noanswer layer
        # with tf.variable_scope("NoAnswer_Layer"):
        #     self.conv_noa = tf.squeeze(conv(tf.concat([self.enc[1], self.enc[2], self.enc[3]],axis = -1),1, bias = False, name = "noanswer_v1"),-1)
        #     # v2 = tf.squeeze(conv(tf.concat([self.enc[1], self.enc[3]],axis = -1),1, bias = False, name = "noanswer_v2"),-1)
        #     # v = tf.concat((v1,v2), axis=-1)
        #     # shp = self.conv_noa.get_shape().as_list()
        #     shp = tf.shape(self.conv_noa)
        #     # self.conv_noa_padded = tf.reshape(pad_up_to(self.conv_noa, [shp[0], 1 * self.config.para_limit]), (self.config.batch_size, 1 * self.config.para_limit))

        #     self.noa_output, _ = BiGRU(inputs = tf.reshape(self.conv_noa, (shp[0], -1, 1 )), 
		# 							sequence_length = None , 
        #                             num_units = 160, 
		# 							is_training = False, 
		# 							keep_prob=0.4,
		# 							scope='BiGRU_noa')
        #     # mask = tf.expand_dims(tf.sequence_mask([shp[1]]*shp[0], self.config.para_limit, dtype=tf.float32), axis=2)
        #     c_mask_float32 = tf.cast(self.c_mask, tf.float32)
        #     self.noa_masked_output = tf.reduce_sum(tf.expand_dims(c_mask_float32, axis=2) * self.noa_output, axis=1) / tf.expand_dims(tf.reduce_sum(c_mask_float32, axis=1), axis=1)

        #     # self.answerable_logits = _linear( tf.nn.relu(_linear(self.noa_masked_output, 80, bias = True, scope='noanswer_layer1') ) , 1, bias=True, scope='noanswer_layer2')
        #     # self.linear_noa_1 =  _linear(self.conv_noa_padded, 80, bias = True, scope='noanswer_layer1')
        #     self.answerable_logits = _linear( self.noa_masked_output , 1, bias=True, scope='linear_noa')  
        #     self.answerable_preds = tf.sigmoid(self.answerable_logits) >= 0.5 #@nguyent279(COMMENT): make this logit for predicting NON-answerability
        
        with tf.variable_scope("Output_Layer"):
            start_logits = tf.squeeze(conv(tf.concat([self.enc[1], self.enc[2]],axis = -1),1, bias = False, name = "start_pointer"),-1)
            end_logits = tf.squeeze(conv(tf.concat([self.enc[1], self.enc[3]],axis = -1),1, bias = False, name = "end_pointer"), -1)
            self.logits = [mask_logits(start_logits, mask = self.c_mask),
                           mask_logits(end_logits, mask = self.c_mask)]

            logits1, logits2 = [l for l in self.logits]
            outer = tf.matmul(tf.expand_dims(tf.nn.softmax(logits1), axis=2), tf.expand_dims(tf.nn.softmax(logits2), axis=1)) # inference: p1_s * p2_e is maximized
            # self.monitor3 = outer 
            # this makes sure that 0 <= end_candidate - start_canidate <= limit; for any (start_candidate, end_candidate) does not satisfy the condition, then
            # outer[j, start_candidate, end_candidate ] = 0
            outer = tf.matrix_band_part(outer, 0, config.ans_limit) # upper triangular matrix (NICE IDEA! but the index condition is not incorporated during the learning but rather as postprocessing)
            # self.monitor4 = outer 
            self.yp1 = tf.argmax(tf.reduce_max(outer, axis=2), axis=1) # the same as: self.py1 = tf.argmax(logits1), but probably tf.matrix_band_part needs such workaround. 
            self.yp2 = tf.argmax(tf.reduce_max(outer, axis=1), axis=1)

            #DEBUG 
            # self.monitor1 = start_logits
            # self.monitor2 = logits1 

            #@thanhnt: Add answerability 
            if self.config.squad_version == '2.0':
                h1 = tf.concat([self.enc[1], self.enc[2]],axis = -1)
                att1 = stable_softmax(logits1) 
                v1 = tf.reduce_sum(tf.expand_dims(att1, 2) * h1, axis=1)  

                h2 = tf.concat([self.enc[1], self.enc[3]],axis = -1)
                att2 = stable_softmax(logits2) 
                v2 = tf.reduce_sum(tf.expand_dims(att2, 2) * h2, axis=1) 

                # attention outputs 
                att_outputs = tf.concat(self.attention_outputs, axis = -1)
                scores_after_att_layer = tf.squeeze(conv(att_outputs, 1), axis=-1)
                scores_after_att_layer = mask_logits(scores_after_att_layer, mask=self.c_mask) #@nguyent279(UPDATE): Masking the logit before computing its softmax (thanks @Soonjae for the notice!) 
                att3 = stable_softmax(scores_after_att_layer)
                v3 = tf.reduce_sum(tf.expand_dims(att3, 2) * att_outputs, axis=1)  

                v = tf.concat([v1, v2, v3], axis=-1)

                self.na_hidden = _linear(v, 80, bias = True, scope='noanswer_layer1')
                self.na_logits = _linear( tf.nn.relu( self.na_hidden ) , self.config.na_dim, bias=True, scope='noanswer_layer2')
                # self.na_logits = tf.squeeze(self.na_logits, axis=1)
                #@thanhnt(AddFeature): Revised objective https://arxiv.org/abs/1710.10723
                # The idea of assuring EPSILON < logits < 0 works. I do so by modifying the revised objective e^z -> e^{2z}, reducting logits by max(logits), and clip values
                answerable_labels = tf.cast(self.answerable_labels, tf.float32)
                # answerable_loss = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(labels=answerable_labels, logits=self.answerable_logits)) 
                # self.loss = answerable_loss

                max_logit = tf.reduce_max(tf.concat( (logits1, logits2, self.na_logits), axis=1 ), axis=1) #(bs,)
                # exp_start = tf.exp( tf.clip_by_value(logits1 - max_logit, VERY_NEGATIVE_BUT_STABLE_ENOUGH, np.inf ) ) 
                exp_start = tf.exp( logits1 - tf.expand_dims(max_logit,axis=1) ) 

                # #DEBUG 
                # # self.monitor = exp_start
                # # print(self.monitor.get_shape().as_list())
                exp_end = tf.exp( logits2 - tf.expand_dims(max_logit,axis=1) )
                # exp_end = tf.exp( tf.clip_by_value(logits2 - max_logit, VERY_NEGATIVE_BUT_STABLE_ENOUGH, np.inf ) )

                if self.config.na_dim == 2:
                    self.answerable_preds = tf.argmax( self.na_logits, axis = 1) # (bs,)
                    exp_answerable = tf.exp( 2 * self.na_logits - 2 * tf.expand_dims(max_logit,axis=1))
                    deno_answerable = tf.reduce_sum(exp_answerable, axis=-1)
                    self.log_numerator = (1- answerable_labels) * 2 * (self.na_logits[:,0] - max_logit) + \
                    answerable_labels * tf.log(  tf.clip_by_value(tf.reduce_sum(self.y1 * exp_start, axis=1) * tf.reduce_sum(self.y2 * exp_end, axis=1) + exp_answerable[:, 1],POSITIVE_BUT_CLOSE_TO_ZERO, np.inf ) ) 
                else:
                    assert config.na_dim == 1
                    self.na_logits = tf.squeeze(self.na_logits, axis=1) # (bs,)
                    self.answerable_preds = tf.sigmoid(self.na_logits) < 0.5

                    exp_answerable = tf.exp( 2 * self.na_logits - 2 * max_logit)
                    deno_answerable = exp_answerable
                    #@nguyent279: add a simple loss for plausible answer; it discourages the plausible logits 
                    self.log_numerator = (1- answerable_labels) * 2 * (self.na_logits - max_logit) + \
                        answerable_labels * (  tf.reduce_sum(self.y1 * (logits1 - tf.expand_dims(max_logit,axis=1)) ,axis=1) + \
                            tf.reduce_sum(self.y2 * (logits2 - tf.expand_dims(max_logit,axis=1)) , axis=1)) +\
                        tf.cast(self.config.use_plausible_ans, tf.float32) * (1- answerable_labels) * (  tf.reduce_sum(self.plausible_y1 * (logits1 - tf.expand_dims(max_logit,axis=1)) ,axis=1) + \
                            tf.reduce_sum(self.plausible_y2 * (logits2 - tf.expand_dims(max_logit,axis=1)) , axis=1))
                    #@nguyent279: the task of NA and answer prediction should be made dedicated; i.e., the answer prediction: "I dont care if the question has NA because that is not my task, so\
                    # every time I see a question and ask myself 'If that question has a NA, what the answer would be?'\
                    # For NA prediction: Now in case of NA questions, I have to share probability mass to the answer prediction too. That forces me to increase my discriminative power to beat with such a \
                    # difficult situation!"

                self.log_denominator = tf.log(  tf.clip_by_value(tf.reduce_sum( exp_start, axis=-1 ) * tf.reduce_sum( exp_end, axis=-1 ) + deno_answerable, POSITIVE_BUT_CLOSE_TO_ZERO, np.inf ) )  
            
                self.monitor = [exp_start, exp_end, exp_answerable, answerable_labels, self.na_logits, max_logit, self.y1, self.y2, logits1, logits2 ]
                self.loss = -tf.reduce_mean(self.log_numerator - self.log_denominator) + \
                        self.config.beta * tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(labels=1 - answerable_labels, logits=self.na_logits))
            
            elif self.config.squad_version == '1.1':
                losses = tf.nn.softmax_cross_entropy_with_logits(
                logits=logits1, labels=self.y1)
                losses2 = tf.nn.softmax_cross_entropy_with_logits(
                logits=logits2, labels=self.y2)
                loss = losses + losses2 
                self.loss = tf.reduce_mean(loss * tf.cast(self.answerable_labels, tf.float32))
                self.answerable_preds = tf.convert_to_tensor(np.array([True] * self.config.batch_size))
            
            else:
                raise('Invalid squad version {}!'.format(self.config.squad_version))

            # #DEBUG
            # self.monitor = [max_logit, 2*self.answerable_logits - 2*max_logit, logits1 - max_logit, logits2 - max_logit]
            # losses = tf.nn.softmax_cross_entropy_with_logits(
            #     logits=logits1, labels=self.y1)
            # losses2 = tf.nn.softmax_cross_entropy_with_logits(
            #     logits=logits2, labels=self.y2)
            # loss = losses + losses2 
            # self.loss = tf.reduce_mean(loss * answerable_labels)

            # answerable_labels = tf.reshape(tf.cast(self.answerable, tf.float32), (self.config.batch_size,1))
            # if self.config.use_answerable:
            #     answerable_loss = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(labels=answerable_labels, logits=self.answerable_logits)) 
            #     if self.config.use_answerable_only:
            #         self.loss = answerable_loss
            #     else:
            #         self.loss = tf.reduce_mean(loss * answerable_labels) + self.config.beta * answerable_loss
            # else: # NO answerability module
            #     self.loss = tf.reduce_mean(loss * answerable_labels)
        
        if config.l2_norm is not None:
            variables = tf.get_collection(tf.GraphKeys.REGULARIZATION_LOSSES)
            l2_loss = tf.contrib.layers.apply_regularization(regularizer, variables)
            self.loss += l2_loss

        if config.decay is not None:
            self.var_ema = tf.train.ExponentialMovingAverage(config.decay)
            ema_op = self.var_ema.apply(tf.trainable_variables())
            with tf.control_dependencies([ema_op]):
                self.loss = tf.identity(self.loss)

                self.assign_vars = []
                for var in tf.global_variables():
                    v = self.var_ema.average(var)
                    if v:
                        self.assign_vars.append(tf.assign(var,v))

    def get_loss(self):
        return self.loss

    def get_global_step(self):
        return self.global_step
