# SAIL@UNIST SQuAD 2.0  
The base code of this model is from [1]. Thanks @NLPLearn for making the implementation publicably available. Note that the base code from [1] is not the official code from the original authors of QANet [2].


# ELMO feature using
* Just clone tensorflow version of ELMO git clone https://github.com/allenai/bilm-tf.git and change accordingly.
* (Updated) There has been some changes in API in bilm. I just pushed the old version of bilm to the master so that you do not have to worry about API mismatch while the master bilm is kept developing!
## Dataset

|                         | squad1.1/train | squad2.0/train | squad1.1/dev | squad2.0/dev | squad1.1/test | squad2.0/test | 
|-------------------------|----------------|----------------|--------------|--------------|---------------|---------------|
|Total examples           |87,599          |130,319         |10,570        |11,873        |9,533          |8,862          | 
|Negative examples        |0               |43,498          |0             |5,945         |0              |4,332          |
| Total articles          |442             |442             |48            |35            |46             |28             |
| Articles with negatives |0               |285             |0             |35            |0              |28             | 

## Performance 
Note that any model that always answers to unanswerable questions has both dev/F1 and dev/EM <= 49.93, and both test/EM and test/F1 <= 51.11. These numbers form an upper bound on the performance of any SQuAD 1.1 model without the capability to detect negative examples. 

|**Model**       | **squad1.1/test/em** | **squad1.1/test/f1**  | **squad2.0/test/em** | **squad2.0/test/f1**  |
|----------------|----------------------|-----------------------|----------------------|-----------------------| 
|Human           | 82.304               | 91.221                | 86.831               |89.452                 |
|Always abstains | -                    | -                     |48.89                 | 48.89                 |
|SOTA            | 84.454               | 90.490                | 71.475               |89.452                 |


## Model development 
| **NA** | **ELMo** | **CharEmb** | **POS+NER** | **Fusion** |**Aug** | **heads**  | **2.0/dev/f1** | **2.0/dev/em**|**2.0/dev/NA_acc**|
|--------|----------|-------------|-------------|------------|--------|------------|----------------|---------------|------------------|
|-       |          |             |             |            |        |            | 49.93          | 49.93         |49.93             |
| x      |  x       |   x         | -           | -          |-       | 8          |68.971          | 65.075        |75.654            |
| x      |  x       |   -         | -           | -          |-       | 8          |67.518          | 63.601        |74.145            |

Note: 
* x : used
* `-` : not used
* empty: not compatible

## References
[1] @NLPLearn. *QANet base code* https://github.com/NLPLearn/QANet. Github   
[2] Adams Wei Yu, David Dohan, Minh-Thang Luong, Rui Zhao, Kai Chen, Mohammad Norouzi, Quoc V. Le. *QANET: COMBINING LOCAL CONVOLUTION WITH GLOBAL SELF-ATTENTION FOR READING COMPREHENSION*.  
