import tensorflow as tf
import random
from tqdm import tqdm
import spacy
import ujson as json
from collections import Counter
import numpy as np
from codecs import open

# Modules for the ELMo features
import os
from bilm_tf.bilm import Batcher, BidirectionalLanguageModel, weight_layers

datadir = os.path.join('bilm_tf','tests', 'fixtures', 'model')
vocab_file = os.path.join(datadir, 'vocab_test.txt')
elmo_options_file = os.path.join(datadir, 'pretrained_options.json')
elmo_weight_file = os.path.join(datadir, 'pretrained_lm_weights.hdf5')


'''
This file is taken and modified from R-Net by HKUST-KnowComp
https://github.com/HKUST-KnowComp/R-Net
'''

nlp = spacy.blank("en")


def word_tokenize(sent):
    doc = nlp(sent)
    return [token.text for token in doc]


def convert_idx(text, tokens):
    current = 0
    spans = []
    for token in tokens:
        current = text.find(token, current)
        if current < 0:
            print("Token {} cannot be found".format(token))
            raise Exception()
        spans.append((current, current + len(token)))
        current += len(token)
    return spans


def process_file(filename, data_type, word_counter, char_counter, pos_counter, ner_counter, nlp, word_limit_allowed=False, para_word_limit = None, ques_word_limit=None, use_corenlp=True):
    """process_file 
    @thanhnt(COMMENT): Add 'answerable' field to the data

    ARGS:
        word_limit_allowed: If True, discard any example which has either paragraph or question word length exceed their corresponding word limit
                            Otherwise (Default), keep all examples (WARNING: The actual maximum paragraph word length may exceed the pre-specified limit config.para_limit)
    """
    print("Generating {} examples...".format(data_type))
    examples = []
    eval_examples = {}
    total = 0
    count_nas = 0 
    count_na_w_plausible = 0
    with open(filename, "r") as fh:
        source = json.load(fh)
        for article in tqdm(source["data"]):
            for para in article["paragraphs"]:
                context = para["context"].replace(
                    "''", '" ').replace("``", '" ')
                #context_tokens = word_tokenize(context)
                if use_corenlp:
                    context_labels = nlp.annotate(context)
                    context_tokens = context_labels['words']
                    context_poss = context_labels['poss']
                    context_ners = context_labels['ners']
                else:
                    context_tokens = word_tokenize(context)
                    context_poss = [0]
                    context_ners = [0]

                if word_limit_allowed:
                    if len(context_tokens) > para_word_limit:
                        continue 
                context_chars = [list(token) for token in context_tokens]
                spans = convert_idx(context, context_tokens)
                for token in context_tokens:
                    word_counter[token] += len(para["qas"])
                    for char in token:
                        char_counter[char] += len(para["qas"])
                for pos, ner in zip(context_poss, context_ners):
                    pos_counter[pos] += len(para["qas"])
                    ner_counter[ner] += len(para["qas"])
                for qa in para["qas"]:
                    total += 1
                    ques = qa["question"].replace(
                        "''", '" ').replace("``", '" ')
                    if use_corenlp:
                        ques_labels = nlp.annotate(ques)
                        #ques_labels = word_tokenize(ques)
                        ques_tokens = ques_labels['words']
                        ques_poss = ques_labels['poss']
                        ques_ners = ques_labels['ners']
                    else:
                        ques_tokens = word_tokenize(ques)
                        ques_poss = [0]
                        ques_ners = [0]
                    
                    if word_limit_allowed:
                        if len(ques_tokens) > ques_word_limit:
                            continue
                    ques_chars = [list(token) for token in ques_tokens]
                    for token in ques_tokens:
                        word_counter[token] += 1
                        for char in token:
                            char_counter[char] += 1
                    for pos, ner in zip(ques_poss, ques_ners):
                        pos_counter[pos] += 1
                        ner_counter[ner] += 1
                    y1s, y2s = [], []
                    answer_texts = []
                    for answer in qa["answers"]:
                        answer_text = answer["text"]
                        answer_start = answer['answer_start']
                        answer_end = answer_start + len(answer_text)
                        answer_texts.append(answer_text)
                        answer_span = []
                        for idx, span in enumerate(spans):
                            if not (answer_end <= span[0] or answer_start >= span[1]):
                                answer_span.append(idx)
                        y1, y2 = answer_span[0], answer_span[-1]
                        y1s.append(y1)
                        y2s.append(y2)

                    #@nguyent279(AddPlausibleAns):
                    plausible_y1s, plausible_y2s = [],[]
                    if len(y1s) == 0:
                        count_nas += 1
                        plausible_answer_texts = []
                        for answer in qa["plausible_answers"]:
                            plausible_answer_text = answer["text"]
                            answer_start = answer['answer_start']
                            answer_end = answer_start + len(plausible_answer_text)
                            plausible_answer_texts.append(plausible_answer_text)
                            answer_span = []
                            for idx, span in enumerate(spans):
                                if not (answer_end <= span[0] or answer_start >= span[1]):
                                    answer_span.append(idx)
                            plausible_y1, plausible_y2 = answer_span[0], answer_span[-1]
                            plausible_y1s.append(plausible_y1)
                            plausible_y2s.append(plausible_y2)
                        if len(plausible_y1s) > 0:
                            count_na_w_plausible += 1
                        # assert len(plausible_y1s) > 0, "Not every NA question has a plausible answer!"
                    answerable = True
                    if len(y1s) == 0:
                        answerable = False
                    
                    #@nguyent279(AddPlausibleAns):
                    example = {"context_tokens": context_tokens, "context_chars": context_chars, "ques_tokens": ques_tokens, 
                               "context_poss": context_poss, "ques_poss": ques_poss, 
							   "context_ners": context_ners, "ques_ners": ques_ners,
                               "ques_chars": ques_chars, 
                               "y1s": y1s, "y2s": y2s, 
                               "plausible_y1s": plausible_y1s,
                               "plausible_y2s":plausible_y2s,
                               "id": total, "answerable": answerable}
                    examples.append(example)
                    eval_examples[str(total)] = {"question": ques,
                        "context": context, "spans": spans, "answers": answer_texts, "uuid": qa["id"]}
        random.shuffle(examples)
        print("{} questions in total".format(len(examples)))
        print("{} out of {} NA questions have an plausible answer".format(count_na_w_plausible, count_nas))
    return examples, eval_examples


def get_embedding(counter, data_type, limit=-1, emb_file=None, size=None, vec_size=None):
    print("Generating {} embedding...".format(data_type))
    embedding_dict = {}
    filtered_elements = [k for k, v in counter.items() if v > limit]
    if emb_file is not None:
        assert size is not None
        assert vec_size is not None
        with open(emb_file, "r", encoding="utf-8") as fh:
            for line in tqdm(fh, total=size):
                array = line.split()
                word = "".join(array[0:-vec_size])
                vector = list(map(float, array[-vec_size:]))
                if word in counter and counter[word] > limit:
                    embedding_dict[word] = vector
        print("{} / {} tokens have corresponding {} embedding vector".format(
            len(embedding_dict), len(filtered_elements), data_type))
    else:
        assert vec_size is not None
        for token in filtered_elements:
            embedding_dict[token] = [np.random.normal(
                scale=0.1) for _ in range(vec_size)]
        print("{} tokens have corresponding embedding vector".format(
            len(filtered_elements)))

    NULL = "--NULL--"
    OOV = "--OOV--"
    token2idx_dict = {token: idx for idx,
                      token in enumerate(embedding_dict.keys(), 2)}
    token2idx_dict[NULL] = 0
    token2idx_dict[OOV] = 1
    embedding_dict[NULL] = [0. for _ in range(vec_size)]
    embedding_dict[OOV] = [0. for _ in range(vec_size)]
    idx2emb_dict = {idx: embedding_dict[token]
                    for token, idx in token2idx_dict.items()}
    emb_mat = [idx2emb_dict[idx] for idx in range(len(idx2emb_dict))]
    return emb_mat, token2idx_dict

def convert_to_features(config, data, word2idx_dict, char2idx_dict):
    """convert_to_features

    @thanhnt(COMMENT): Adapt the function to "answerable" field
    """

    example = {}
    context, question = data
    context = context.replace("''", '" ').replace("``", '" ')
    question = question.replace("''", '" ').replace("``", '" ')
    example['context_tokens'] = word_tokenize(context)
    example['ques_tokens'] = word_tokenize(question)
    example['context_chars'] = [list(token) for token in example['context_tokens']]
    example['ques_chars'] = [list(token) for token in example['ques_tokens']]

    para_limit = config.test_para_limit
    ques_limit = config.test_ques_limit
    ans_limit = 100
    char_limit = config.char_limit

    def filter_func(example):
        if example["answerable"]:
            return len(example["context_tokens"]) > para_limit or \
                len(example["ques_tokens"]) > ques_limit

    if filter_func(example):
        raise ValueError("Context/Questions lengths are over the limit")

    context_idxs = np.zeros([para_limit], dtype=np.int32)
    context_char_idxs = np.zeros([para_limit, char_limit], dtype=np.int32)
    ques_idxs = np.zeros([ques_limit], dtype=np.int32)
    ques_char_idxs = np.zeros([ques_limit, char_limit], dtype=np.int32)
    y1 = np.zeros([para_limit], dtype=np.float32)
    y2 = np.zeros([para_limit], dtype=np.float32)
    #@nguyent279(AddPlausibleAns):
    plausible_y1 = np.zeros([para_limit], dtype=np.float32)
    plausible_y2 = np.zeros([para_limit], dtype=np.float32)

    def _get_word(word):
        for each in (word, word.lower(), word.capitalize(), word.upper()):
            if each in word2idx_dict:
                return word2idx_dict[each]
        return 1

    def _get_char(char):
        if char in char2idx_dict:
            return char2idx_dict[char]
        return 1

    for i, token in enumerate(example["context_tokens"]):
        context_idxs[i] = _get_word(token)

    for i, token in enumerate(example["ques_tokens"]):
        ques_idxs[i] = _get_word(token)

    for i, token in enumerate(example["context_chars"]):
        for j, char in enumerate(token):
            if j == char_limit:
                break
            context_char_idxs[i, j] = _get_char(char)

    for i, token in enumerate(example["ques_chars"]):
        for j, char in enumerate(token):
            if j == char_limit:
                break
            ques_char_idxs[i, j] = _get_char(char)

    return context_idxs, context_char_idxs, ques_idxs, ques_char_idxs

def build_features(config, examples, data_type, out_file, word2idx_dict, char2idx_dict, pos2idx_dict, ner2idx_dict, is_test=False):
    """build_features
    """
    para_limit = config.test_para_limit if is_test else config.para_limit
    ques_limit = config.test_ques_limit if is_test else config.ques_limit
    ans_limit = 100 if is_test else config.ans_limit
    char_limit = config.char_limit
    # char_to_id for the ELMo
    para_batcher = Batcher(vocab_file, config.ELMo_limit, para_limit)
    ques_batcher = Batcher(vocab_file, config.ELMo_limit, ques_limit)

    def filter_func(example, is_test=False):
        if example["answerable"]:
            return len(example["context_tokens"]) > para_limit or \
                len(example["ques_tokens"]) > ques_limit or \
                (example["y2s"][0] - example["y1s"][0]) > ans_limit
        #@nguyent279(AddPlausibleAns)
        else:
            if len(example["plausible_y1s"]) > 0: # not every NA question has a plausible answer
                return len(example["context_tokens"]) > para_limit or \
                    len(example["ques_tokens"]) > ques_limit or \
                    (example["plausible_y2s"][0] - example["plausible_y1s"][0]) > ans_limit


    print("Processing {} examples...".format(data_type))
    writer = tf.python_io.TFRecordWriter(out_file)
    total = 0
    total_ = 0
    meta = {}
    for example in tqdm(examples):
        total_ += 1

        #@thanhnt(COMMENT): Discard any example whose word or char lengths exceeds their limits
        #@thanhnt(TODO): Remove such a filter duplicated at 'process_file'!
        if filter_func(example, is_test): 
            continue

        total += 1
        #thanhnt(COMMENT): Zero padding to the limit size. 
        context_idxs = np.zeros([para_limit], dtype=np.int32)
        context_char_idxs = np.zeros([para_limit, char_limit], dtype=np.int32)
        ques_idxs = np.zeros([ques_limit], dtype=np.int32)
        ques_char_idxs = np.zeros([ques_limit, char_limit], dtype=np.int32)
        #@nguyent279(AddPlausibleAns)
        plausible_y1 = np.zeros([para_limit], dtype=np.float32)
        plausible_y2 = np.zeros([para_limit], dtype=np.float32) 
        y1 = np.zeros([para_limit], dtype=np.float32)
        y2 = np.zeros([para_limit], dtype=np.float32)
        context_pos_idxs = np.zeros([para_limit], dtype=np.int32)
        ques_pos_idxs = np.zeros([ques_limit], dtype=np.int32)
        context_ner_idxs = np.zeros([para_limit], dtype=np.int32)
        ques_ner_idxs = np.zeros([ques_limit], dtype=np.int32)
        
        def _get_word(word):
            for each in (word, word.lower(), word.capitalize(), word.upper()):
                if each in word2idx_dict:
                    return word2idx_dict[each]
            return 1

        def _get_char(char):
            if char in char2idx_dict:
                return char2idx_dict[char]
            return 1
        def _get_pos(pos):
            if pos in pos2idx_dict:
                return pos2idx_dict[pos]
            return 1
        def _get_ner(ner):
            if ner in ner2idx_dict:
                return ner2idx_dict[ner]
            return 1
            
        
        for i, token in enumerate(example["context_tokens"]):
            context_idxs[i] = _get_word(token)

        for i, token in enumerate(example["ques_tokens"]):
            ques_idxs[i] = _get_word(token)

        for i, token in enumerate(example["context_chars"]):
            for j, char in enumerate(token):
                if j == char_limit:
                    break
                context_char_idxs[i, j] = _get_char(char)

        for i, token in enumerate(example["ques_chars"]):
            for j, char in enumerate(token):
                if j == char_limit:
                    break
                ques_char_idxs[i, j] = _get_char(char)
        for i, token in enumerate(example["context_poss"]):
            context_pos_idxs[i] = _get_pos(token)
        for i, token in enumerate(example["ques_poss"]):
            ques_pos_idxs[i] = _get_pos(token)
        for i, token in enumerate(example["context_ners"]):
            context_ner_idxs[i] = _get_ner(token)
        for i, token in enumerate(example["ques_ners"]):
            ques_ner_idxs[i] = _get_ner(token)



        if example["answerable"]:
            start, end = example["y1s"][-1], example["y2s"][-1]
            y1[start], y2[end] = 1.0, 1.0
        #@nguyent279(AddPlausibleAns)
        else:
            if len(example["plausible_y1s"]) > 0:
                start, end = example["plausible_y1s"][-1], example["plausible_y2s"][-1]
                plausible_y1[start], plausible_y2[end] = 1.0, 1.0

        # else: do nothing

        # Get ELMo features
        ELMo_context_char_idxs = para_batcher.batch_sentences([example["context_tokens"]])[0]
        ELMo_ques_char_idxs = ques_batcher.batch_sentences([example["ques_tokens"]])[0]
        #print(example["context_tokens"])
        #print(np.array(context_char_idxs).shape)
        #print(np.array(ELMo_context_char_idxs).shape)
        #print(np.array(ques_char_idxs).shape)
        #print(np.array(ELMo_ques_char_idxs).shape)
        record = tf.train.Example(features=tf.train.Features(feature={
                                  "context_idxs": tf.train.Feature(bytes_list=tf.train.BytesList(value=[context_idxs.tostring()])),
                                  "ques_idxs": tf.train.Feature(bytes_list=tf.train.BytesList(value=[ques_idxs.tostring()])),
                                  "context_char_idxs": tf.train.Feature(bytes_list=tf.train.BytesList(value=[context_char_idxs.tostring()])),
                                  "ques_char_idxs": tf.train.Feature(bytes_list=tf.train.BytesList(value=[ques_char_idxs.tostring()])),
                                  "y1": tf.train.Feature(bytes_list=tf.train.BytesList(value=[y1.tostring()])),
                                  "y2": tf.train.Feature(bytes_list=tf.train.BytesList(value=[y2.tostring()])),
                                  #@nguyent279(AddPlausibleAns):
                                  "plausible_y1": tf.train.Feature(bytes_list=tf.train.BytesList(value=[plausible_y1.tostring()])),
                                  "plausible_y2": tf.train.Feature(bytes_list=tf.train.BytesList(value=[plausible_y2.tostring()])),
                                  "id": tf.train.Feature(int64_list=tf.train.Int64List(value=[example["id"]])),
                                  "answerable": tf.train.Feature(int64_list=tf.train.Int64List(value=[example["answerable"]])),
                                  # ELMo features
                                  "ELMo_context_char_idxs": tf.train.Feature(bytes_list=tf.train.BytesList(value=[ELMo_context_char_idxs.tostring()])),  
                                  "ELMo_ques_char_idxs": tf.train.Feature(bytes_list=tf.train.BytesList(value=[ELMo_ques_char_idxs.tostring()])),
								  # Additional features
								  "context_pos_idx": tf.train.Feature(bytes_list=tf.train.BytesList(value=[context_pos_idxs.tostring()])),
								  "ques_pos_idx": tf.train.Feature(bytes_list=tf.train.BytesList(value=[ques_pos_idxs.tostring()])),
								  "context_ner_idx": tf.train.Feature(bytes_list=tf.train.BytesList(value=[context_ner_idxs.tostring()])),
								  "ques_ner_idx": tf.train.Feature(bytes_list=tf.train.BytesList(value=[ques_ner_idxs.tostring()])),
                                  }))
        writer.write(record.SerializeToString())
    print("Built {} / {} instances of features in total".format(total, total_))
    meta["total"] = total
    writer.close()
    return meta

def save(filename, obj, message=None):
    if message is not None:
        print("Saving {}...".format(message))
        with open(filename, "w") as fh:
            json.dump(obj, fh)


def prepro(config):
    if config.use_corenlp:
        # Modules for the corenlp
        from text_analyzer import text_analyzer
        nlp = text_analyzer(config)
    else:
        nlp = None

    word_counter, char_counter = Counter(), Counter()
    pos_counter, ner_counter = Counter(), Counter()
    train_examples, train_eval = process_file(
        config.train_file, "train", word_counter, char_counter, pos_counter, ner_counter, nlp, word_limit_allowed=True, para_word_limit=config.para_limit, ques_word_limit=config.ques_limit, use_corenlp=config.use_corenlp)
    dev_examples, dev_eval = process_file(
        config.dev_file, "dev", word_counter, char_counter, pos_counter, ner_counter, nlp, word_limit_allowed=True, para_word_limit=config.para_limit, ques_word_limit=config.ques_limit,use_corenlp=config.use_corenlp)
    test_examples, test_eval = process_file(
        config.test_file, "test", word_counter, char_counter, pos_counter, ner_counter, nlp, word_limit_allowed=True, para_word_limit=config.para_limit, ques_word_limit=config.ques_limit,use_corenlp=config.use_corenlp)

    word_emb_file = config.fasttext_file if config.fasttext else config.glove_word_file
    char_emb_file = config.glove_char_file if config.pretrained_char else None
    char_emb_size = config.glove_char_size if config.pretrained_char else None
    char_emb_dim = config.glove_dim if config.pretrained_char else config.char_dim
    pos_emb_file = None
    pos_emb_size = None
    pos_emb_dim = 50
    ner_emb_file = None
    ner_emb_size = None
    ner_emb_dim = 50

    word_emb_mat, word2idx_dict = get_embedding(
        word_counter, "word", emb_file=word_emb_file, size=config.glove_word_size, vec_size=config.glove_dim)
    char_emb_mat, char2idx_dict = get_embedding(
        char_counter, "char", emb_file=char_emb_file, size=char_emb_size, vec_size=char_emb_dim)
    pos_emb_mat, pos2idx_dict = get_embedding(
        pos_counter, "pos", emb_file=pos_emb_file, size=pos_emb_size, vec_size=pos_emb_dim)
    ner_emb_mat, ner2idx_dict = get_embedding(
        ner_counter, "ner", emb_file=ner_emb_file, size=ner_emb_size, vec_size=ner_emb_dim)

    build_features(config, train_examples, "train",
                   config.train_record_file, word2idx_dict, char2idx_dict, pos2idx_dict, ner2idx_dict)
    dev_meta = build_features(config, dev_examples, "dev",
                              config.dev_record_file, word2idx_dict, char2idx_dict, pos2idx_dict, ner2idx_dict)
    test_meta = build_features(config, test_examples, "test",
                               config.test_record_file, word2idx_dict, char2idx_dict, pos2idx_dict, ner2idx_dict, is_test=True)

    save(config.word_emb_file, word_emb_mat, message="word embedding")
    save(config.char_emb_file, char_emb_mat, message="char embedding")
    save(config.pos_emb_file, pos_emb_mat, message="pos embedding")
    save(config.ner_emb_file, ner_emb_mat, message="ner embedding")
    save(config.train_eval_file, train_eval, message="train eval")
    save(config.dev_eval_file, dev_eval, message="dev eval")
    save(config.test_eval_file, test_eval, message="test eval")
    save(config.dev_meta, dev_meta, message="dev meta")
    save(config.test_meta, test_meta, message="test meta")
    save(config.word_dictionary, word2idx_dict, message="word dictionary")
    save(config.char_dictionary, char2idx_dict, message="char dictionary")
    save(config.pos_dictionary, pos2idx_dict, message="pos dictionary")
    save(config.ner_dictionary, ner2idx_dict, message="ner dictionary")
